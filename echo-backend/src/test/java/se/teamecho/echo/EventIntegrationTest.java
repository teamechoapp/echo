//package se.teamecho.echo;
//
//import static org.mockito.Matchers.any;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.UUID;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import se.teamecho.echo.controller.EventController;
//import se.teamecho.echo.domain.Event;
//import se.teamecho.echo.service.EventService;
//
//public class EventIntegrationTest {
//
//    private MockMvc mockMvc;
//
//    @InjectMocks
//    private EventController eventController;
//
//    @Mock
//    private EventService eventService;
//
//    private String uuid = UUID.randomUUID().toString();
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//
//        this.mockMvc = MockMvcBuilders.standaloneSetup(eventController)
//                .setMessageConverters(new MappingJackson2HttpMessageConverter())
//                .build();
//    }
//
//    @Test
//    public void getAllEvent() throws Exception {
//        mockMvc.perform(get("/events")).andExpect(status().isOk());
//    }
//
//    @Test
//    public void getOneEventNoneExists() throws Exception {
//        this.mockMvc.perform(get("/events/{id}", uuid))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    public void getOneEventExists() throws Exception {
//        this.mockMvc.perform(get("/events/{id}", uuid))
//                .andExpect(status().isOk());
//    }
// }
