package se.teamecho.echo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import se.teamecho.echo.domain.Category;

@Repository
public interface CategoryRepository extends MongoRepository<Category, String> {}
