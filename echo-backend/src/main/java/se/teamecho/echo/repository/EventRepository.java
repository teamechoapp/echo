package se.teamecho.echo.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import se.teamecho.echo.domain.Event;

@Repository
public interface EventRepository extends MongoRepository<Event, String> {

    List<Event> findByEndDateGreaterThan(Sort sort, String datetime);
}
