package se.teamecho.echo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;

@Configuration
@ComponentScan
@PropertySource({ "classpath:mongodb.properties" })
@EnableMongoRepositories
public class AppConfiguration extends AbstractMongoConfiguration {

    @Autowired
    private Environment env;

    @Override
    protected String getDatabaseName() {
        return env.getProperty("mongodb.database");
    }

    @Bean
    @Override
    public Mongo mongo() throws Exception {
        MongoClient client = new MongoClient(env.getProperty("mongodb.host"));
        // Throw exceptions on write operation failures.
        client.setWriteConcern(WriteConcern.SAFE);
        return client;
    }

    @Override
    protected UserCredentials getUserCredentials() {
        return new UserCredentials(env.getProperty("mongodb.username"), env.getProperty("mongodb.password"));
    }
}
