package se.teamecho.echo.domain;

import java.util.TreeMap;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * An event object.
 * 
 * @See 
 *      http://docs.spring.io/spring-data/mongodb/docs/1.2.x/reference/html/mapping
 *      -chapter.html for mappers.
 * 
 * @author Emil Stjerneman
 * 
 */
@Document(collection = "event")
public class Event {

    @Id
    private String id;

    private String name;

    private String description;

    @DBRef
    private Category category;

    private double latitude;

    private double longitude;

    private String startDate;

    private String endDate;

    private String address;

    private String ownerEmail;

    private TreeMap<String, Profile> attendees;

    public Event () {
        attendees = new TreeMap<String, Profile>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public TreeMap<String, Profile> getAttendees() {
        return attendees;
    }

    public void setAttendees(TreeMap<String, Profile> attendees) {
        this.attendees = attendees;
    }

    public void addAttendance(Profile profile) {
        getAttendees().put(profile.getId(), profile);
    }

    public void removeAttendance(Profile profile) {
        try {
            getAttendees().remove(profile.getId());
        }
        catch (NullPointerException e) {

        }
    }
}
