package se.teamecho.echo.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import se.teamecho.echo.domain.Event;
import se.teamecho.echo.domain.Profile;
import se.teamecho.echo.service.EventService;

@RestController
@RequestMapping("/events")
public class EventController {

    private final static Logger log = Logger.getLogger(EventController.class);

    @Autowired
    private EventService service;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Event>> getEvents() {
        log.info("EventController - GetAll");
        return service.getActiveOrderedEvents();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Event> getEvent(@PathVariable String id) {
        log.info("EventController - GetOne");
        return service.getEvent(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Event> addEvent(@RequestBody Event event) {
        log.info("EventController - Add");
        return service.addEvent(event);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Event> updateEvent(@PathVariable String id, @RequestBody Event event) {
        log.info("EventController - Put");
        log.info("event to put" + event.toString());
        return service.updateEvent(id, event);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Event> deleteCategory(@PathVariable String id) {
        log.info("EventController - Delete");
        return service.removeEvent(id);
    }

    @RequestMapping(value = "/{id}/attend/add", method = RequestMethod.POST)
    public ResponseEntity<Event> addAttendance(@PathVariable String id, @RequestBody Profile profile) {
        log.info("EventController - Add attend");
        return service.addAttendance(id, profile);
    }

    @RequestMapping(value = "/{id}/attend/remove", method = RequestMethod.POST)
    public ResponseEntity<Event> removeAttendance(@PathVariable String id, @RequestBody Profile profile) {
        log.info("EventController - Remove attend");
        return service.removeAttendance(id, profile);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleClientErrors(Exception ex) {
        log.error(ex.getMessage(), ex);
        return ex.getMessage();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleServerErrors(Exception ex) {
        log.error(ex.getMessage(), ex);
        return ex.getMessage();
    }
}
