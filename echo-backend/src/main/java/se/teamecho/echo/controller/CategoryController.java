package se.teamecho.echo.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import se.teamecho.echo.domain.Category;
import se.teamecho.echo.service.CategoryService;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private final static Logger log = Logger.getLogger(CategoryController.class);

    @Autowired
    private CategoryService service;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Category>> getCategories() {
        log.info("CategoryController - GetAll");
        return service.getCategories();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Category> getCategory(@PathVariable String id) {
        log.info("CategoryController - GetOne");
        return service.getCategory(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Category> addCategory(@RequestBody Category category) {
        log.info("CategoryController - Add");
        return service.addCategory(category);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Category> updateCategory(@PathVariable String id, @RequestBody Category category) {
        log.info("CategoryController - Put");
        log.info("Category to put" + category.toString());
        return service.updateCategory(id, category);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Category> deleteCategory(@PathVariable String id) {
        log.info("CategoryController - Delete");
        return service.removeCategory(id);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handleClientErrors(Exception ex) {
        log.error(ex.getMessage(), ex);
        return ex.getMessage();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleServerErrors(Exception ex) {
        log.error(ex.getMessage(), ex);
        return ex.getMessage();
    }
}
