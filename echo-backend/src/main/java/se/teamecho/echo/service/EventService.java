package se.teamecho.echo.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import se.teamecho.echo.domain.Event;
import se.teamecho.echo.domain.Profile;

public interface EventService {

    public ResponseEntity<List<Event>> getEvents();

    public ResponseEntity<Event> getEvent(String id);

    public ResponseEntity<Event> addEvent(Event event);

    public ResponseEntity<Event> updateEvent(String id, Event event);

    public ResponseEntity<Event> removeEvent(String id);

    public ResponseEntity<List<Event>> getActiveOrderedEvents();

    public ResponseEntity<Event> addAttendance(String id, Profile profile);

    public ResponseEntity<Event> removeAttendance(String id, Profile profile);
}
