package se.teamecho.echo.service;

import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.joda.time.MutableDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import se.teamecho.echo.domain.Event;
import se.teamecho.echo.domain.Profile;
import se.teamecho.echo.repository.EventRepository;

@Service
public class EventServiceImpl implements EventService {

    private final static Logger log = Logger.getLogger(EventServiceImpl.class);

    @Autowired
    private EventRepository repo;

    @Override
    public ResponseEntity<List<Event>> getEvents() {
        log.info("Fetching all events");

        List<Event> events = repo.findAll();

        return new ResponseEntity<List<Event>>(events, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Event> getEvent(String id) {
        log.info("Fetching event with id: " + id);

        if (!repo.exists(id)) {
            log.warn("Fetching failed - Didn't find event with id: " + id);
            return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Event>(repo.findOne(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Event> addEvent(Event event) {
        log.info("Adding event: " + event.toString());

        event.setId(UUID.randomUUID().toString());

        Event saved_event = repo.save(event);

        return new ResponseEntity<Event>(saved_event, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Event> updateEvent(String id, Event event) {
        if (!repo.exists(id)) {
            log.warn("Update failed - Didn't find source event with id: " + id);
            return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
        }

        // Force the event to have the same ID as the request have.
        event.setId(id);

        Event updated_event = repo.save(event);
        return new ResponseEntity<Event>(updated_event, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Event> removeEvent(String id) {
        log.info("Deleting event with id: " + id);

        if (!repo.exists(id)) {
            log.warn("Delete failed - Didn't find event with id: " + id);
            return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
        }

        repo.delete(id);

        return new ResponseEntity<Event>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Event>> getActiveOrderedEvents() {
        log.info("Fetching all active events in order by time");

        MutableDateTime dateTime = new MutableDateTime();

        List<Event> events = repo.findByEndDateGreaterThan(new Sort("startDate"), dateTime.toDateTime().toString());

        return new ResponseEntity<List<Event>>(events, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Event> addAttendance(String id, Profile profile) {
        if (!repo.exists(id)) {
            log.warn("Add attendance - Didn't find source event with id: " + id);
            return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
        }

        Event event = repo.findOne(id);
        event.addAttendance(profile);

        Event updated_event = repo.save(event);
        return new ResponseEntity<Event>(updated_event, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Event> removeAttendance(String id, Profile profile) {
        if (!repo.exists(id)) {
            log.warn("Remove attendance - Didn't find source event with id: " + id);
            return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
        }

        Event event = repo.findOne(id);
        event.removeAttendance(profile);

        Event updated_event = repo.save(event);
        return new ResponseEntity<Event>(updated_event, HttpStatus.OK);
    }
}
