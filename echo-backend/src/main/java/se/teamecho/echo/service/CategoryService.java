package se.teamecho.echo.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import se.teamecho.echo.domain.Category;

public interface CategoryService {

    public ResponseEntity<List<Category>> getCategories();

    public ResponseEntity<Category> getCategory(String id);

    public ResponseEntity<Category> addCategory(Category category);

    public ResponseEntity<Category> updateCategory(String id, Category category);

    public ResponseEntity<Category> removeCategory(String id);

}
