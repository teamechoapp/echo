package se.teamecho.echo.service;

import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import se.teamecho.echo.domain.Category;
import se.teamecho.echo.repository.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final static Logger log = Logger.getLogger(CategoryServiceImpl.class);

    @Autowired
    private CategoryRepository repo;

    @Override
    public ResponseEntity<List<Category>> getCategories() {
        log.info("Fetching all categories");
        List<Category> events = repo.findAll();

        return new ResponseEntity<List<Category>>(events, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Category> getCategory(String id) {
        log.info("Fetching category with id: " + id);

        if (!repo.exists(id)) {
            log.warn("Fetching failed - Didn't find category with id: " + id);
            return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Category>(repo.findOne(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Category> addCategory(Category category) {
        log.info("Adding category: " + category.toString());

        category.setId(UUID.randomUUID().toString());

        Category saved_category = repo.save(category);

        return new ResponseEntity<Category>(saved_category, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Category> updateCategory(String id, Category category) {
        if (!repo.exists(id)) {
            log.warn("Update failed - Didn't find source category with id: " + id);
            return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
        }

        // Force the event to have the same ID as the request have.
        category.setId(id);

        Category updated_category = repo.save(category);
        return new ResponseEntity<Category>(updated_category, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Category> removeCategory(String id) {
        log.info("Deleting category with id: " + id);

        if (!repo.exists(id)) {
            log.warn("Delete failed - Didn't find category with id: " + id);
            return new ResponseEntity<Category>(HttpStatus.NOT_FOUND);
        }

        repo.delete(id);

        return new ResponseEntity<Category>(HttpStatus.OK);
    }

}
