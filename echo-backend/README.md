# Echo App Backend

Vill man inte köra med vår publika server kan man sätta upp en egen.

* * *

## INSTALLATION

### INSTALLEAR MONGODB
Databasen som vår server använder är en mongodb databas. Se http://www.mongodb.org/ för mer information.

För att installera och starta mongodb läs följande kapteil:

* *"Install MongoDB"*
* *"Run MongoDB"* 

dessa finns i länkarna här under.

**Du behöver inte sätta några permissions och sådant skit, bara se till att det rullar med default mongo-konfigurationen.**

#### PÅ WINDOWS
Som vanligt är det jävligt krångligt att göra detta på Windows så håll ut.

Se http://docs.mongodb.org/manual/tutorial/install-mongodb-on-windows/

#### PÅ LINUX
Som vanligt är det jävligt lätt att göra detta på Linux.

Se http://docs.mongodb.org/manual/administration/install-on-linux/

#### PÅ OSX
Det borde bara som vanligt, d.v.s. jävligt enkelt.

Se http://docs.mongodb.org/manual/tutorial/install-mongodb-on-os-x/

* * *

## KÖR EGEN SERVER

### MED .WAR-FILEN.
**Se till att du har mongodb installerat**

Ladda ner *echo-backend-1.x-SprintX.war* från [vårt bitbucket repo](https://bitbucket.org/teamechoapp/echo/downloads) och 
deploya till Tomcat på lämpligt sätt.

### FRÅN KÄLLKODEN.

Klona detta repo.
```sh
git clone git@bitbucket.org:teamechoapp/echo.git
cd echo
```

Kopiera *default-mongodb.properties* och spara till en ny fil med namnet *mongodb.properties* i samma mapp.
```sh
cp echo-backend/src/main/resources/default-mongodb.properties echo-backend/src/main/resources/mongodb.properties
```
Editera sedan denna filen med dina mongoDB inställningar.

Gå till echo-backend
```sh
cd echo-backend
```

Starta echo-backend
```sh
./run_server
```
eller kör 
```sh
mvn spring-boot:run
```

### STÄLL IN IP I APPEN 

Första gången du öppnar appen kommer det ett felmeddelande "Error while requesting server".
Detta beror på att appen inte hittar serven. 

För att ställa in IP-numret till servern så går du via menyn i appen till "Settings".
Där fyller du i IP-numret för din dator.

Hur du hittar detta får du googla själv.

För att detta skall fungera så krävs det självklart att din dator och telefon ligger på samma nät och har access till varandra.
Skolans när fungerar **INTE**. Kör hotspot via din telefon för att få det att fungera.