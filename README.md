# ECHO - Lets get toghter

## PRODUCTER

### Echo Android
Klientapplikationen körs på enheter med Android (API 16+). Appen kommuncerar med serverapplikationen, så ingen data lagras hos klienten. 

### Echo Backend (Server)

**Du behöver INTE köra igång någon backend för att komma igång, default använder appen vår publika server, men vill du köra en egen server går detta också**

Detta är serverapplikationen. Den körs med hjälp av [Spring boot](http://projects.spring.io/spring-boot/) och den inbäddade
Tomcat servern. Data sparas i en [mongoDB](https://www.mongodb.org/)-databas*. 

\**mongoDB-databasen följer inte med serven utan behövs installeras manuellt.* 

* * *

## BEROENDEN
För att kunna köra applikationen fullt ut krävs följande beroenden av:

** Klient **

* Android API 16+
* Internetuppkoppling
* Google Play installerad på enheten.

** Server ** (om du sätter upp en egen) 

* mongoDB

* * *

## INSTALLATION

För att kunna köra APK-filen **måste** man köra denna på en enhet som har Google Play installerat.
Detta har de flesta riktiga enheter.

Detta betyder då också att du **INTE** kan köra applikationen på en "vanlig" genymotion.

Applikationen behöver också en backend att kommunicera med, vi har en publik server som den använder som standard, men det går även att installera sin egen server.

### (INSTALLERA GOOGLE PLAY I GENYMOTYION)
Planerar du att installera och köpa applikationen via Genymotion behöver du installera Google Play Service i den virtuella miljön.

För att kunna installera Google Play Service i genymotion kräva det att man har **Genymotion 2.2+** och **VirtualBox 4.2+**.

* Ladda sedan ner dessa två filerna till din dator:
 * [ARM Translation Installer v1.1](http://filetrip.net/dl?4SUOrdcMRv)
 * [Google Apps for Android 4.2](http://goo.im/gapps/gapps-jb-20130812-signed.zip)
* Skapa sedan en ny device i genymotion. Förslagsvis **Galaxy Nexus-4.2.2 with google apps-Api 17 -720x1280** med API 4.2.2.
* Öppna din nya device och gå till hemskärmen.
* Drag n droppa **Genymotion-ARM-Translation_v1.1.zip** filen in i genymotion.

  Den kommer då säga *"File transfer in progress"*, och när den ber dig "flasha" så tryck **OK**.
* Starta om genymotion.
* Drag n droppa **gapps-jb-20130812-signed.zip** filen in i genymotion.

  Den kommer då säga *"File transfer in progress"*, och när den ber dig "flasha" så tryck **OK**.
* Starta om genymotion igen.
* Nu har du installerat Google Play Service i din genymotion device. Logga in med ditt google-konto för att aktivera det.

Nu kan du köra appen med kartan och allt gött i sig.


* * *

## KÖR


### KÖR APK

Installera den som du gör med alla andra APK filer.

Det kan hända att du behöver sätta "Tillåt falska platser" i din telefon för att det skall fungera, detta märker du.


## För dig som är 1337, sno gärna vår kod.

Klona detta repo.
```sh
git clone git@bitbucket.org:teamechoapp/echo.git
cd echo
```

* * *

# Changelog

## 1.x-Sprint5 (1.0)

* Lagt till möjlighet att attenda events.
* Nya kategorifärger.
* Förbättrat validering vid skapade av events.
* Logga in med Google Plus+.
* Logga ut från appen.
* Tagit bort onödiga toasts.
* Lagt till rättigheter på events så bara den som skapat eventet kan ändra och ta bort det.
* Lagt till en preview-map inne på event sidan.
* Refactorerat string.xml filen.
* Lagt till en kartvy där man ser vart man själva befinner sig i relations till eventet.
* Man kan nu uppdatera sina egna events.
* Man kan nu ta bort sina egna events.
* Användare har numera sin egen profilsida med information om sitt konto.
* Man kan nu växla karttyp på alla kartor.
* Lagt till toast då man klickar på kartan för att sätta eventes position så man ser vilken adrss "pluppen" har.

## 1.x-Sprint4
**Klient**

* Hämta kategorier från serven.
* Kategorier har en färg-representation
* Implementerat en indikator som visas om det inte finns några events i listan.
* Lagt till adress för events.
* Lagt till en dedikerad sida för ett event med fullständig information om detta event.
* Lagt till en karta som visar samtliga events.
* Lagt till möjlighet att specificera tid för ett event.

**Server**

* Stöd för kategorier.

## 1.x-Sprint3.1

**Klient**

* Added info window to show version number.

## 1.x-Sprint3
**Klient**

* Då man skapar ett nytt event kan man ange position via en karta.
* Då man skapar ett nytt event kan man ange en tid.
* Lagt till installning för vilken HOST-adress appen skall fråga efter data på.

**Server**

* Lagt till stöd för eventtid.
* Snyggat till debug loggar.

## 1.x-Sprint2
**Klient**

* Lista events i en lista
* Skapa events med grunläggade information så som namn, beskrivning, och kategori.

**Server**

* Hämta flera events
* Hämga enskillt event
* Spara events

# Maintainers

* [Christian Karlsson](https://bitbucket.org/Somnium)
* [Paul Lachenardière](https://bitbucket.org/paullachenardiere)
* [Emil Stjerneman](https://bitbucket.org/bratanon)