package se.teamecho.echoapp.adapters;

import se.teamecho.echoapp.R;
import se.teamecho.echoapp.domain.Event;
import se.teamecho.echoapp.utils.DateAndTime;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class EventListAdapter extends ArrayAdapter<Event> {

    public EventListAdapter (Context context) {
        super(context, R.layout.event_list_item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Event event = this.getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.event_list_item, parent, false);
        }

        TextView event_name = (TextView) convertView.findViewById(R.id.event_item_name);
        TextView event_category = (TextView) convertView.findViewById(R.id.event_item_category);
        TextView event_time = (TextView) convertView.findViewById(R.id.event_item_date);

        event_name.setText(event.getName());
        event_category.setBackgroundColor(Color.parseColor(event.getCategory().getBackgroundColor()));
        event_category.setTextColor(Color.parseColor(event.getCategory().getTextColor()));
        event_category.setText(event.getCategory().getName());

        String event_start = DateAndTime.getDateAndTimeNiceName(event.getStartDateDateTime());
        String event_end = DateAndTime.getDateAndTimeNiceName(event.getEndDateDateTime());

        event_time.setText(event_start + " - " + event_end);

        return convertView;
    }
}
