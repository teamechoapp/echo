package se.teamecho.echoapp.adapters;

import se.teamecho.echoapp.domain.Category;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class CategoryListAdapter extends ArrayAdapter<Category> implements SpinnerAdapter {

    private LayoutInflater inflator;

    private Context context;
    private int resource;
    private int textViewResource;

    public CategoryListAdapter (Context context, int resource, int textViewResource) {
        super(context, resource, textViewResource);

        this.context = context;
        this.resource = resource;
        this.textViewResource = textViewResource;

        init();
    }

    private void init() {
        inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            convertView = inflator.inflate(resource, parent, false);
        }

        Category category = this.getItem(position);

        TextView textView = (TextView) convertView.findViewById(textViewResource);
        textView.setText(category.getName());
        textView.setTextColor(Color.parseColor(category.getTextColor()));
        textView.setBackgroundColor(Color.parseColor(category.getBackgroundColor()));
        textView.setPadding(10, 10, 10, 10);

        ViewGroup.MarginLayoutParams margins = (MarginLayoutParams) textView.getLayoutParams();
        margins.setMargins(10, 15, 10, 15);

        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflator.inflate(resource, parent, false);
        }

        TextView textView = (TextView) convertView.findViewById(textViewResource);
        textView.setText(this.getItem(position).getName());

        return convertView;
    }

}
