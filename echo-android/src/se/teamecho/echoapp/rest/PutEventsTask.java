package se.teamecho.echoapp.rest;

import se.teamecho.echoapp.domain.Event;
import se.teamecho.echoapp.domain.Profile;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

/**
 * Put an event to the server.
 * 
 * @author Emil Stjerneman
 * 
 */
public class PutEventsTask extends RESTRequestTask<Event, Void, Boolean> {

    private final static String TAG = "PutEventsTask";

    public PutEventsTask (Context context, String path) {
        super(context, path);
    }

    @Override
    protected Boolean doInBackground(Event... params) {
        super.doInBackground(params);

        // Add owner
        Event event_to_out = params[0];
        event_to_out.setOwnerEmail(Profile.getInstance().getEmail());

        try {
            restTemplate.put(url, event_to_out);
            return true;
        }
        catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (result) {
            Activity activity = (Activity) this.context;
            activity.finish();
        }

        // Remove the dialog.
        progressDialog.dismiss();
    }
}
