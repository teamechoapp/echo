package se.teamecho.echoapp.rest;

import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import se.teamecho.echoapp.R;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Loads data from a server with REST.
 * 
 * @author Emil Stjerneman
 * 
 */
public abstract class RESTRequestTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    protected final static String TAG = "RESTRequestTask";

    private final int READ_TIMEOUT = 10000;

    private final int CONNECTION_TIMEOUT = 5000;

    protected ProgressDialog progressDialog;

    protected final String url;

    protected Context context;

    protected RestTemplate restTemplate;

    /**
     * Construct a new REST request task.
     * 
     * @param context
     *            the context for this request.
     * @param path
     *            the path to request.
     */
    public RESTRequestTask (Context context, String path) {
        this.context = context;

        // Get settings from shared preferences.
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.context);
        String hostname = sharedPref.getString("hostname", "54.187.236.74:8080/echo");

        // Construct the URL.
        this.url = "http://" + hostname + "/" + path;
    }

    @Override
    protected Result doInBackground(Params... params) {
        Log.d(TAG, "Requestning server with hostname : " + url);

        restTemplate = new RestTemplate();
        SimpleClientHttpRequestFactory requestFactory = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
        requestFactory.setReadTimeout(READ_TIMEOUT);
        requestFactory.setConnectTimeout(CONNECTION_TIMEOUT);

        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        return null;
    }

    @Override
    protected void onPreExecute() {
        // Add the progress dialog.
        progressDialog = ProgressDialog.show(context, "", context.getString(R.string.task_body_loading));
    }

    @Override
    protected void onPostExecute(Result result) {
        // Handle null results.
        if (result == null) {
            Log.e(TAG, "Error while requsting server");
            Toast.makeText(context, context.getString(R.string.task_error), Toast.LENGTH_LONG).show();
        }

        // Remove the dialog.
        progressDialog.dismiss();
    }
}
