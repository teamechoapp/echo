package se.teamecho.echoapp.rest;

import se.teamecho.echoapp.domain.Event;
import se.teamecho.echoapp.domain.Profile;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

/**
 * Post an event to the server.
 * 
 * @author Emil Stjerneman
 * 
 */
public class PostEventsTask extends RESTRequestTask<Event, Void, Event> {

    private final static String TAG = "PostEventsTask";

    public PostEventsTask (Context context) {
        super(context, "events");
    }

    @Override
    protected Event doInBackground(Event... params) {
        super.doInBackground(params);

        // Add owner
        Event event_to_post = params[0];
        event_to_post.setOwnerEmail(Profile.getInstance().getEmail());

        try {
            Event event = restTemplate.postForObject(url, event_to_post, Event.class);
            return event;
        }
        catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Event result) {
        super.onPostExecute(result);

        if (result != null) {
            Activity activity = (Activity) this.context;
            activity.finish();
        }
    }
}
