package se.teamecho.echoapp.rest;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class DeleteEventTask extends RESTRequestTask<Void, Void, Void> {

    private final static String TAG = "DeleteEventTask";

    public DeleteEventTask (Context context, String path) {
        super(context, path);
    }

    @Override
    protected Void doInBackground(Void... params) {
        super.doInBackground(params);

        try {
            restTemplate.delete(url);
        }
        catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        Log.i(TAG, "Event deleted");
        Toast.makeText(context, "Event deleted", Toast.LENGTH_LONG).show();
    }

}
