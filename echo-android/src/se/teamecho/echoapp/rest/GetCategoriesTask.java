package se.teamecho.echoapp.rest;

import se.teamecho.echoapp.domain.Category;
import android.content.Context;
import android.util.Log;

/**
 * Load all categories from the server.
 * 
 * @author Emil Stjerneman
 * 
 */
public class GetCategoriesTask extends RESTRequestTask<Void, Void, Category[]> {

    private final static String TAG = "GetCategoriesTask";

    private final static String DEFUALT_PATH = "categories";

    public GetCategoriesTask (Context context) {
        super(context, DEFUALT_PATH);
    }

    public GetCategoriesTask (Context context, String path) {
        super(context, path);
    }

    @Override
    protected Category[] doInBackground(Void... params) {
        super.doInBackground(params);
        Category[] categories = null;

        try {
            categories = restTemplate.getForObject(url, Category[].class);
        }
        catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return categories;
    }

    @Override
    protected void onPostExecute(Category[] categories) {
        super.onPostExecute(categories);
    }
}
