package se.teamecho.echoapp.rest;

import se.teamecho.echoapp.domain.Event;
import se.teamecho.echoapp.domain.Profile;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * When a user attends to unattends an event this class will make the requests.
 * 
 * @author Emil Stjerneman
 * 
 */
public class AttendEventsTask extends RESTRequestTask<Void, Void, Void> {

    private final static String TAG = "AttendEventsTask";

    public enum ACTION {
        ATTEND, UNATTEND
    }

    private ACTION action;

    public AttendEventsTask (Context context, String path, ACTION action) {
        super(context, path);
        this.action = action;
    }

    @Override
    protected Void doInBackground(Void... params) {
        super.doInBackground(params);

        try {
            restTemplate.postForObject(url, Profile.getInstance(), Event.class);
        }
        catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        // Remove the dialog.
        progressDialog.dismiss();

        if (action == ACTION.ATTEND) {
            Toast.makeText(this.context, "You have attened the event.", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this.context, "You have unattened the event.", Toast.LENGTH_SHORT).show();
        }
    }
}
