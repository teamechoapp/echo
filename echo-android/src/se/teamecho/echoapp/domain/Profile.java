package se.teamecho.echoapp.domain;

import java.io.Serializable;

import android.graphics.Bitmap;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Profile POJO. Keeps track of a user.
 * 
 * 
 * @author Emil Stjerneman
 * 
 */
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;

    private static Profile instace = null;

    private String id;
    private String name;
    private String email;

    @JsonIgnore
    private Bitmap image;

    private Profile () {}

    public static Profile getInstance() {
        if (instace == null) {
            instace = new Profile();
        }

        return instace;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
