package se.teamecho.echoapp.domain;

import java.io.Serializable;
import java.util.TreeMap;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.android.gms.maps.model.LatLng;

public class Event implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;

    private String description;

    private Category category;

    private Double latitude;

    private Double longitude;

    private String startDate;

    private String endDate;

    private String address;

    private String ownerEmail;

    private TreeMap<String, Profile> attendees;

    public Event () {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonIgnore
    public LatLng getLatLng() {
        return new LatLng(getLatitude(), getLongitude());
    }

    public String getStartDate() {
        return startDate;
    }

    @JsonIgnore
    public DateTime getStartDateDateTime() {
        return DateTime.parse(getStartDate());
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    @JsonIgnore
    public DateTime getEndDateDateTime() {
        return DateTime.parse(getEndDate());
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public TreeMap<String, Profile> getAttendees() {
        return attendees;
    }

    public void setAttendees(TreeMap<String, Profile> attendees) {
        this.attendees = attendees;
    }
}
