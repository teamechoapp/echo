package se.teamecho.echoapp.domain;

import java.io.Serializable;

/**
 * An event category pojo.
 * 
 * @author Emil Stjerneman
 * 
 */
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;

    private String backgroundColor;

    private String textColor;

    public Category () {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    @Override
    public String toString() {
        return "Category [id=" + id + ", name=" + name + ", backgroundColor=" + backgroundColor + ", textColor=" + textColor + "]";
    }
}
