package se.teamecho.echoapp.activity;

import se.teamecho.echoapp.fragments.CreateEventFragment;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

public class CreateEventActivity extends Activity {
    private final static String TAG = "CreateEventActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
            .add(android.R.id.content, new CreateEventFragment()).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        
        switch (id) {
        case android.R.id.home:
            Log.d(TAG, "Home");
            showWarning();    
            break;    
        }
        return super.onOptionsItemSelected(item);
    }

    boolean choice;
    Intent intent;
    public boolean showWarning() {
        new AlertDialog.Builder(this)
        .setTitle("Warning!")
        .setMessage("Are you sure you want to return? The event will NOT be saved")
        .setIcon(getResources().getDrawable(se.teamecho.echoapp.R.drawable.echologo))
        .setIcon(android.R.drawable.ic_popup_reminder)
        .setPositiveButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int answer) { 
                choice = false;
            }
        })
        .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int answer) { 
                choice = true;
                intent = new Intent(CreateEventActivity.this, ListEventsActivity.class);
                startActivity(intent);
                finish();
            }
        })
        .show();
        return choice;

    }

    @Override
    public void onBackPressed() {
         showWarning();
    }

}
