package se.teamecho.echoapp.activity;

import se.teamecho.echoapp.R;
import se.teamecho.echoapp.fragments.ListEventsFragment;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ListEventsActivity extends Activity {

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

    private final static String TAG = "ListEventActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
            .add(android.R.id.content, new ListEventsFragment()).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.list_event, menu);
        getMenuInflater().inflate(R.menu.show_events_on_map, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        getMenuInflater().inflate(R.menu.about_echo, menu);
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;

        switch (id) {
        case android.R.id.home:
            Log.d(TAG, "Home");
            intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            finish();
            return true;  
        case R.id.menu_settings:
            intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            break;

        case R.id.menu_create_event:
            intent = new Intent(this, CreateEventActivity.class);
            startActivity(intent);
            return true;

        case R.id.menu_show_events_on_map:
            intent = new Intent(this, AllEventsMapActivity.class);
            startActivity(intent);
            return true;

        case R.id.menu_profile:
            intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            return true;

        case R.id.menu_abount_echo:
            Dialog d = new Dialog(this, R.style.AboutEcho);
            d.setContentView(R.layout.about_echo);
            d.show();
            TextView findViewById = (TextView) d.findViewById(R.id.about_version);

            PackageInfo pinfo;
            try {
                pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                findViewById.setText(pinfo.versionName);
            }
            catch (NameNotFoundException e) {
                Log.e(TAG, "Error reading version from manifest.");
            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
