package se.teamecho.echoapp.activity;

import se.teamecho.echoapp.fragments.EventDetailsFragment;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

public class EventDetailsActivity extends Activity {
    private final static String TAG = "EventDetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        final ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);
        
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(android.R.id.content, new EventDetailsFragment())
                    .commit();
        }
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
        case android.R.id.home:
            Log.d(TAG, "Home");
            intent = new Intent(this, ListEventsActivity.class);
            startActivity(intent);
            finish();
            break;
        }
        return super.onOptionsItemSelected(item);
    }
}
