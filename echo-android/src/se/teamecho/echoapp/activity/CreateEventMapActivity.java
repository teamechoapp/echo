package se.teamecho.echoapp.activity;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import se.teamecho.echoapp.R;
import se.teamecho.echoapp.domain.Category;
import se.teamecho.echoapp.utils.ColorConverter;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class CreateEventMapActivity extends Activity implements OnMapClickListener, OnMarkerDragListener {

    private final static String TAG = "MapPositionActivity";

    private final static int ZOOM = 13;

    private GoogleMap map;
    private Marker marker;
    private MenuItem menu_use_button;

    private Location currentLocation;
    private Location lastKnownLocation;
    private LocationManager locationManager;
    private LocationListener locationListener;
    public boolean loadAddressDone = true;

    public String address;

    private float hue = 0;

    private AsyncTask<LatLng, Void, String> execute = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);
        
        

        setContentView(R.layout.map_fragment);
        currentLocation = getCurrentLocation();

        setUpMapIfNeeded();

        Bundle extras = getIntent().getExtras();

        if (getIntent().hasExtra("category")) {
            Category category = (Category) extras.getSerializable("category");
            hue = ColorConverter.fakeHueValue(category.getName());
        }

        if (getIntent().hasExtra("location")) {
            LatLng latlng = (LatLng) extras.getParcelable("location");
            addCurrentMark(latlng);
        }
        else {
            if (currentLocation != null) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), ZOOM));
            }
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed()");
        locationManager.removeUpdates(locationListener);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (execute != null && execute.getStatus() == Status.RUNNING) {
            execute.cancel(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.event_map_type, menu);
        getMenuInflater().inflate(R.menu.create_event_map, menu);
        menu_use_button = menu.findItem(R.id.menu_create_event_map_use);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
        case android.R.id.home:
            Log.d(TAG, "Home");
            onBackPressed();
            break;
        case R.id.menu_create_event_map_cancel:
            Log.d(TAG, "Cancel map");
            this.setResult(RESULT_CANCELED);
            finish();
            break;
        case R.id.menu_create_event_map_use:
            Log.d(TAG, "Use map");
            Intent intent = new Intent();
            intent.putExtra("location", marker.getPosition());
            intent.putExtra("address", address);
            this.setResult(RESULT_OK, intent);
            finish();
            break;
        case R.id.menu_create_event_map_normal:
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            Log.d(TAG, "NORMAL map view");
            break;
        case R.id.menu_create_event_map_hybrid:
            map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            Log.d(TAG, "HYBRID map view");
            break;
        case R.id.menu_create_event_map_terrain:
            map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            Log.d(TAG, "TERRAIN map view");
            break;
        case R.id.menu_create_event_map_satellite:
            map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            Log.d(TAG, "SATELLITE map view");
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the
        // map.
        if (map == null) {
            // Try to obtain the map from the SupportMapFragment.
            map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (map != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setMyLocationEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setRotateGesturesEnabled(true);
        map.setOnMapClickListener(this);
        map.setOnMarkerDragListener(this);
    }

    @Override
    public void onMapClick(LatLng point) {
        placeMarker(point);
    }

    private void placeMarker(LatLng point) {
        // Clean the map from all markers.
        map.clear();
        marker = map.addMarker(new MarkerOptions()
        .position(point)
        .draggable(true)
        .icon(BitmapDescriptorFactory.defaultMarker(hue)));
        map.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()), 500, null);

        if (execute == null || (execute != null && execute.getStatus() != Status.RUNNING)) {
            execute = new GetAddressTask(this).execute(point);
        }
        else if (execute != null && execute.getStatus() == Status.RUNNING) {
            execute.cancel(true);
        }
    }

    public void addCurrentMark(LatLng point) {
        // Clean the map from all markers.
        map.clear();
        marker = map.addMarker(new MarkerOptions()
        .position(point)
        .draggable(true)
        .icon(BitmapDescriptorFactory.defaultMarker(hue)));

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), ZOOM));
    }

    private Location getCurrentLocation() {

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {}

            @Override
            public void onProviderEnabled(String provider) {
                currentLocation = getCurrentLocation();
                Toast.makeText(CreateEventMapActivity.this, "GPS is on", Toast.LENGTH_SHORT).show();
                if (currentLocation != null) {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), ZOOM));
                }
            }

            @Override
            public void onProviderDisabled(String provider) {
                Toast.makeText(CreateEventMapActivity.this, "GPS is off. Swich it on!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLocationChanged(Location location) {
                currentLocation = location;
                locationManager.removeUpdates(this);
            }
        };
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (currentLocation != null) {
            return currentLocation;
        }

        else if (currentLocation == null && lastKnownLocation != null) {
            return lastKnownLocation;
        }
        else {
            return null;
        }
    }

    /**
     * Get an address by the given location.
     * 
     * @author Emil Stjerneman
     * 
     */
    @SuppressLint("DefaultLocale")
    private class GetAddressTask extends AsyncTask<LatLng, Void, String> {

        private final static String TAG = "GetAddressTask";

        private Context context;

        public GetAddressTask (Context context) {
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            menu_use_button.setEnabled(false);
        }

        @SuppressLint({ "ShowToast", "DefaultLocale" })
        @Override
        protected String doInBackground(LatLng... params) {
            if (!loadAddressDone) {
                Toast.makeText(CreateEventMapActivity.this, address, Toast.LENGTH_LONG).cancel();
            }

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            // Get the current location from the input parameter list
            LatLng loc = params[0];
            // Create a list to contain the result address
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(loc.latitude, loc.longitude, 1);
            }
            catch (IOException e1) {
                Log.e(TAG, "IO Exception in getFromLocation()");
                return ("IO Exception trying to get address");
            }
            catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(loc.latitude) +
                        " , " +
                        Double.toString(loc.longitude) +
                        " passed to address service";
                Log.e(TAG, errorString);
                return errorString;
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available), city, and
                 * country name.
                 */
                String countryName = address.getCountryName();
                if (countryName == null) {
                    countryName = "";
                }

                String locality = address.getLocality();
                if (locality == null) {
                    locality = "";
                }

                String addressText = "";
                if (address.getMaxAddressLineIndex() > 0) {
                    if (address.getAddressLine(0) != null) {
                        addressText += address.getAddressLine(0);
                    }
                }

                if (address.getLocality() != null) {
                    addressText += ", " + address.getLocality();
                }

                if (address.getCountryName() != null) {
                    addressText += ", " + address.getCountryName().toUpperCase();
                }

                return addressText;
            }
            else {
                loadAddressDone = false;
                return "No address found";
            }
        }

        @Override
        protected void onPostExecute(String newAddress) {
            address = newAddress;

            if (loadAddressDone) {
                Toast.makeText(CreateEventMapActivity.this, address, Toast.LENGTH_LONG).show();
            }
            loadAddressDone = true;

            if (!(newAddress.isEmpty())) {
                menu_use_button.setEnabled(true);
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            loadAddressDone = false;
            menu_use_button.setEnabled(true);
        }
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        map.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()), 2000, null);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        Log.i(TAG, "Marker drag ended. Setting location");
        placeMarker(marker.getPosition());
    }

    @Override
    public void onMarkerDragStart(Marker marker) {}
}
