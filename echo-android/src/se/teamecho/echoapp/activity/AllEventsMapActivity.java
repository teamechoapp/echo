package se.teamecho.echoapp.activity;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import se.teamecho.echoapp.R;
import se.teamecho.echoapp.domain.Event;
import se.teamecho.echoapp.rest.RESTRequestTask;
import se.teamecho.echoapp.utils.ColorConverter;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class AllEventsMapActivity extends Activity
implements OnMarkerClickListener, GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener,
GoogleMap.OnMapClickListener{

    private final static String TAG = "ShowAllEventsOnMapActivity";
    private final static int ZOOM = 13;
    private GoogleMap map;
    private MarkerOptions marker;
    public MarkerOptions addNewMarker;
    private Map<Marker, Event> allEventsLatLng = new HashMap<Marker, Event>();
    private View infoView;
    private Event event;
    boolean allEventsMode = true;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private LatLng currentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);
        setContentView(R.layout.map_fragment);
        currentLocation = getCurrentLocation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.event_map_type, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        if (getIntent().hasExtra("event")) {
            allEventsMode = false;
            Bundle extras = getIntent().getExtras();
            event = (Event) extras.getSerializable("event");
            addNewMarker = addNewMarker(event.getLatitude(), event.getLongitude());

            addNewMarker.icon(BitmapDescriptorFactory.defaultMarker(ColorConverter.fakeHueValue(event.getCategory().getName())));
            Marker newMarker = map.addMarker(addNewMarker);
            allEventsLatLng.put(newMarker, event);
        }
        else {
            allEventsMode = true;
            new GetEventsTask(this, "events").execute();
        }

        setUpMapSettings();
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed()");
        locationManager.removeUpdates(locationListener);
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
        case android.R.id.home:
            Log.d(TAG, "Home");
            onBackPressed();
            break;
        case R.id.menu_create_event_map_normal:
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            Log.d(TAG, "NORMAL map view");
            break;
        case R.id.menu_create_event_map_hybrid:
            map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            Log.d(TAG, "HYBRID map view");
            break;
        case R.id.menu_create_event_map_terrain:
            map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            Log.d(TAG, "TERRAIN map view");
            break;
        case R.id.menu_create_event_map_satellite:
            map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            Log.d(TAG, "SATELLITE map view");
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpMapSettings() {
        map.setMyLocationEnabled(true);

        if (!allEventsMode) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(event.getLatLng(), 16), 2000, null);
        }

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setOnMarkerClickListener(this);
        map.setOnInfoWindowClickListener(this);
        map.setOnMapClickListener(this);

    }

    private MarkerOptions addNewMarker(Double lat, Double lng) {
        marker = new MarkerOptions().position(new LatLng(lat, lng));
        return marker;
    }

    Polyline addPolyline;

    private void drawLine(Event eve, LatLng latlng) {

        if(addPolyline != null && addPolyline.isVisible()){
            addPolyline.remove();
        }
        //if location is null, no line is printed (is GPS is off...)
        if(getCurrentLocation() != null) {
            String color = eve.getCategory().getBackgroundColor();
            PolylineOptions geodesic = new PolylineOptions()
            .add(getCurrentLocation(), latlng)
            .width(4)
            .color(Color.parseColor(color))
            .geodesic(true);
            addPolyline = map.addPolyline(geodesic);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Set<Marker> keySet = allEventsLatLng.keySet();
        for (Marker m : keySet) {
            if (!marker.equals(m)) {
                m.setAlpha(0.4f);
            }
            else {
                m.setAlpha(1);
                event = allEventsLatLng.get(m);
            }
        }
        drawLine(event, marker.getPosition());
        map.setInfoWindowAdapter(this);
        return false;
    }

    @Override
    public View getInfoContents(Marker marker) {
        event = allEventsLatLng.get(marker);
        infoView = getLayoutInflater().inflate(R.layout.event_info_window_adapter, null);
        infoView.setClickable(true);

        infoView.setLayoutParams(new RelativeLayout.LayoutParams(500, RelativeLayout.LayoutParams.WRAP_CONTENT));

        TextView name = (TextView) infoView.findViewById(R.id.map_info_window_event_name);
        TextView category = (TextView) infoView.findViewById(R.id.map_info_window_event_category);

        name.setText(event.getName());

        category.setText(event.getCategory().getName());

        return infoView;

    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public void onInfoWindowClick(Marker arg0) {
        Intent intent = new Intent(AllEventsMapActivity.this, EventDetailsActivity.class);
        intent.putExtra("event", event);
        startActivity(intent);
        finish();
    }

    private LatLng getCurrentLocation() {

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {}

            @Override
            public void onProviderEnabled(String provider) {
                currentLocation = getCurrentLocation();
                Toast.makeText(AllEventsMapActivity.this, "GPS is on", Toast.LENGTH_SHORT).show();
                if (currentLocation != null) {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, ZOOM));
                }
            }

            @Override
            public void onProviderDisabled(String provider) {
                Toast.makeText(AllEventsMapActivity.this, "GPS is off. Swich it on!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLocationChanged(Location location) {
                currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                locationManager.removeUpdates(this);
            }
        };
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (currentLocation != null) {
            return currentLocation;
        }

        else if (currentLocation == null && lastKnownLocation != null) {
            return currentLocation = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
        }
        else {
            return null;
        }
    }

    /**
     * Load events from the server.
     * 
     * @author Emil Stjerneman
     * 
     */
    private class GetEventsTask extends RESTRequestTask<Void, Void, Event[]> {

        public GetEventsTask (Context context, String path) {
            super(context, path);
        }

        @Override
        protected Event[] doInBackground(Void... params) {
            super.doInBackground(params);
            Event[] events = null;

            try {
                events = restTemplate.getForObject(url, Event[].class);
                return events;
            }
            catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
            return events;
        }

        @Override
        protected void onPostExecute(Event[] events) {
            super.onPostExecute(events);
            if (events != null) {
                for (Event e : events) {
                    addNewMarker = addNewMarker(e.getLatitude(), e.getLongitude());
                    addNewMarker.icon(BitmapDescriptorFactory.defaultMarker(ColorConverter.fakeHueValue(e.getCategory().getName())));
                    Marker newMarker = map.addMarker(addNewMarker);
                    allEventsLatLng.put(newMarker, e);
                }
            }
        }
    }

    @Override
    public void onMapClick(LatLng arg0) {
        if(addPolyline != null && addPolyline.isVisible())
            addPolyline.remove();

        Set<Marker> keySet = allEventsLatLng.keySet();
        for (Marker ma : keySet) {
            ma.setAlpha(1);
        }
    }
}
