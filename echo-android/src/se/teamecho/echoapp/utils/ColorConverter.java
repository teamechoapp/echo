package se.teamecho.echoapp.utils;

/**
 * Converts different color types to another.
 * 
 * @author Emil Stjerneman
 * 
 */
public class ColorConverter {

    /**
     * Convert a HEX color to a RGB value.
     * 
     * @param hex
     *            the hex color to convert.
     * @return an an array representing a RGB value.
     */
    public static int[] hexToRgb(String hex) {

        if (hex.startsWith("#")) {
            hex = hex.substring(1);
        }

        int color = (int) Long.parseLong(hex, 16);

        int[] rgb_values = new int[3];

        rgb_values[0] = (color >> 16) & 0xFF;
        rgb_values[1] = (color >> 8) & 0xFF;
        rgb_values[2] = (color >> 0) & 0xFF;

        return rgb_values;
    }

    /**
     * Converts a HEX color to Hue value.
     * 
     * @param hex
     *            the hex color to convert.
     * @return the hue value.
     */
    public static float hexToHue(String hex) {
        int[] rgb = ColorConverter.hexToRgb(hex);

        /**
         * http://developer.android.com/reference/android/graphics/Color.html
         * 
         * hsv[0] is Hue [0 .. 360) hsv[1] is Saturation [0...1] hsv[2] is Value
         * [0...1]
         * 
         * This is WRONG!!
         * 
         * hsv[1] is Hue.
         */
        // float[] hsv = new float[3];
        // Color.RGBToHSV(rgb[0], rgb[1], rgb[2], hsv);

        // double test = Math.atan2(1.732050808 * (rgb[1] - rgb[2]), (2 *
        // (rgb[0] - rgb[1] - rgb[2]))) * 57.295779513;
        //
        // if (test < 0) {
        // test = 360 + test;
        // }
        //
        // Log.e("COLOR", String.valueOf((float) test));

        return 0;
    }

    /**
     * Fake hue values as it seems impossible to calculate them.
     */
    public static float fakeHueValue(String name) {
        switch (name) {
            case "BBQ":
                return 0;
            case "Geek meet":
                return 30;
            case "Political":
                return 330;
            case "Festivals":
                return 210;
            case "Seeking new friends":
                return 120;
            case "Date":
                return 330;
            case "Music":
                return 240;
            case "Networking":
                return 120;
            case "Art / Culture":
                return 270;
            case "Other":
                return 60;
            default:
                return 0;

        }
    }
}
