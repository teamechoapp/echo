package se.teamecho.echoapp.utils;

import java.util.ArrayList;
import java.util.List;

import se.teamecho.echoapp.domain.Event;
import android.app.AlertDialog;
import android.util.Log;

/**
 * Validation class for the Event object. When user creates an event it checks
 * if anything is null or empty. Returns true if validation succeed.
 * 
 * @author Paul Lachenardiere
 */

public class EventValidation {

    private final static String TAG = "EventValidation";

    public static boolean eventIsValidated(Event event, Boolean isEditMode, AlertDialog.Builder builder) {
        List<String> errors = new ArrayList<>();

        if (event.getName().isEmpty()) {
            Log.i(TAG, "Name can't be empty");
            errors.add("Name can't be empty");
        }

        // if (event.getDescription().isEmpty()) {
        // Log.i(TAG, "Description can't be empty");
        // return false;
        // }

        // if (event.getAddress().isEmpty()) {
        // Log.i(TAG, "Address can't be empty");
        // return false;
        // }

        if (event.getCategory() == null) {
            Log.i(TAG, "Category can't be empty");
            errors.add("Category can't be empty");
        }

        if (event.getLatitude() == null || event.getLongitude() == null) {
            Log.i(TAG, "Location can't be empty");
            errors.add("Location can't be empty");
        }

        if (!isEditMode && EventValidation.isStartDateBeforeNow(event)) {
            Log.i(TAG, "Start date is in the past");
            errors.add("Start date is in the past");
        }

        if (!isEditMode && EventValidation.isEndDateBeforeStartDate(event)) {
            Log.i(TAG, "End date is before start date");
            errors.add("End date is before start date");
        }

        if (errors.size() > 0) {
            String errorString = "";

            for (String error : errors) {
                errorString += error + "\n";
            }
            builder.setMessage(errorString);

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

        return errors.size() > 0 ? false : true;
    }

    public static boolean isStartDateBeforeNow(Event event) {
        return event.getStartDateDateTime().isBeforeNow();
    }

    public static boolean isEndDateBeforeStartDate(Event event) {
        return event.getEndDateDateTime().isBefore(event.getStartDateDateTime());
    }
}
