package se.teamecho.echoapp.utils;

import org.joda.time.MutableDateTime;
import org.joda.time.base.BaseDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Help class for parsing Date and Time objects.
 * 
 * @author Paul Lachenardiere
 */
public class DateAndTime {

    /**
     * Returns a short nice date and time string.
     */
    public static String getDateAndTimeNiceName(BaseDateTime date) {
        DateTimeFormatter format = DateTimeFormat.forPattern("HH:mm, d MMMM, yyyy");

        return date.toString(format);
    }

    /**
     * Returns a short nice date string.
     */
    public static String getDateNiceName(MutableDateTime date) {
        DateTimeFormatter format = DateTimeFormat.forPattern("E dd MMM y");

        return date.toString(format);
    }

    /**
     * Returns a short nice time string.
     */
    public static String getTimeNiceName(MutableDateTime date) {
        DateTimeFormatter format = DateTimeFormat.forPattern("HH:mm");

        return date.toString(format);
    }

    public MutableDateTime parseDateAndTimeToObject(String dateAndTime) {
        MutableDateTime parse = MutableDateTime.parse(dateAndTime);
        return parse;
    }
}
