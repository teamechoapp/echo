package se.teamecho.echoapp.fragments;

import java.io.InputStream;
import java.util.Map.Entry;

import se.teamecho.echoapp.R;
import se.teamecho.echoapp.activity.AllEventsMapActivity;
import se.teamecho.echoapp.domain.Category;
import se.teamecho.echoapp.domain.Event;
import se.teamecho.echoapp.domain.Profile;
import se.teamecho.echoapp.rest.AttendEventsTask;
import se.teamecho.echoapp.rest.AttendEventsTask.ACTION;
import se.teamecho.echoapp.utils.DateAndTime;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.model.LatLng;

public class EventDetailsFragment extends Fragment {

    ImageView staticGoogleMap;
    Event event = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.event_details, container, false);

        TextView event_name = (TextView) rootView.findViewById(R.id.event_name);
        TextView event_description = (TextView) rootView.findViewById(R.id.event_description);
        TextView event_category = (TextView) rootView.findViewById(R.id.event_category);
        TextView event_address = (TextView) rootView.findViewById(R.id.event_address);

        TextView event_start_date = (TextView) rootView.findViewById(R.id.event_start_date);
        TextView event_end_date = (TextView) rootView.findViewById(R.id.event_end_date);

        staticGoogleMap = (ImageView) rootView.findViewById(R.id.static_google_map);
        staticGoogleMap.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (event != null) {
                    Intent intent = new Intent(getActivity(), AllEventsMapActivity.class);
                    intent.putExtra("event", event);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        if (getActivity().getIntent().hasExtra("event")) {
            Bundle extras = getActivity().getIntent().getExtras();
            event = (Event) extras.getSerializable("event");
        }

        if (event == null) {
            Toast.makeText(getActivity(), "Did not find selected event.", Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }

        new GetStaticGoogleMap().execute(event);

        LinearLayout top_container = (LinearLayout) rootView.findViewById(R.id.event_top_container);
        top_container.setBackgroundColor(Color.parseColor(event.getCategory().getBackgroundColor()));

        event_name.setText(event.getName());
        event_name.setTextColor(Color.parseColor(event.getCategory().getTextColor()));

        event_category.setText(event.getCategory().getName());

        event_start_date.setText(DateAndTime.getDateAndTimeNiceName(event.getStartDateDateTime()));
        event_start_date.setTextColor(Color.parseColor(event.getCategory().getTextColor()));

        event_end_date.setText(DateAndTime.getDateAndTimeNiceName(event.getEndDateDateTime()));
        event_end_date.setTextColor(Color.parseColor(event.getCategory().getTextColor()));

        event_address.setText(event.getAddress());

        if (event.getDescription().length() == 0) {
            event_description.setVisibility(View.GONE);
        }
        event_description.setText(event.getDescription());

        ToggleButton attendButton = (ToggleButton) rootView.findViewById(R.id.btn_attend_event);

        if (event.getAttendees().containsKey(Profile.getInstance().getId())) {
            attendButton.setChecked(true);
        }

        TextView event_attendees_list_label = (TextView) rootView.findViewById(R.id.event_attendees_list_label);
        event_attendees_list_label.setText(getString(R.string.event_attendees_list_label) + " (" + event.getAttendees().size() + ")");

        attendButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    new AttendEventsTask(getActivity(), "events/" + event.getId() + "/attend/add", ACTION.ATTEND).execute();
                }
                else {
                    new AttendEventsTask(getActivity(), "events/" + event.getId() + "/attend/remove", ACTION.UNATTEND).execute();
                }
            }
        });

        final LinearLayout event_attendees_list = (LinearLayout) rootView.findViewById(R.id.event_attendees_list);

        for (Entry<String, Profile> entry : event.getAttendees().entrySet()) {
            TextView attendee = new TextView(getActivity());
            attendee.setText(entry.getValue().getName());
            event_attendees_list.addView(attendee);
        }

        return rootView;
    }

    private class GetStaticGoogleMap extends AsyncTask<Event, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Event... event) {
            int width = 1000;
            int height = 650;
            Log.i(getTag(), String.valueOf(width));

            Event evt = event[0];
            LatLng loc = evt.getLatLng();
            Category category = evt.getCategory();
            String color = category.getBackgroundColor().substring(1);

            double latitude = loc.latitude;
            double longitude = loc.longitude;
            String url = "http://maps.google.com/maps/api/staticmap?size=" + width + "x" + height
                    + "&markers=color:0x" + color + "%7Clabel:%7C" + latitude + "," + longitude + "&maptype=roadmap&sensor=true";
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            }
            catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            staticGoogleMap.setImageBitmap(bitmap);
        }
    }
}
