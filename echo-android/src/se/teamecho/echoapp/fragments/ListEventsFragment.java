package se.teamecho.echoapp.fragments;

import se.teamecho.echoapp.R;
import se.teamecho.echoapp.activity.AllEventsMapActivity;
import se.teamecho.echoapp.activity.CreateEventActivity;
import se.teamecho.echoapp.activity.EventDetailsActivity;
import se.teamecho.echoapp.adapters.EventListAdapter;
import se.teamecho.echoapp.domain.Event;
import se.teamecho.echoapp.domain.Profile;
import se.teamecho.echoapp.rest.DeleteEventTask;
import se.teamecho.echoapp.rest.RESTRequestTask;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ListEventsFragment extends Fragment {

    private EventListAdapter adapter;
    private ListView list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.event_list, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        initialize();
        new GetEventsTask(getActivity(), "events").execute();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        ListView list = (ListView) v;

        AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
        Event event = (Event) list.getItemAtPosition(info.position);

        menu.setHeaderTitle(event.getName());

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.event_context_menu, menu);

        if (!event.getOwnerEmail().equals(Profile.getInstance().getEmail())) {
            menu.removeItem(R.id.action_event_item_context_edit);
            menu.removeItem(R.id.action_event_item_context_delete);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

        Event event = adapter.getItem(info.position);
        Intent intent;

        switch (item.getItemId()) {
            case R.id.action_event_item_context_edit:
                intent = new Intent(getActivity(), CreateEventActivity.class);
                intent.putExtra("event", event);
                startActivity(intent);
                return true;
            case R.id.action_event_item_context_delete:
                new DeleteEventTask(getActivity(), "events/" + event.getId()).execute();
                // TODO: Reload list
                return true;
            case R.id.action_event_item_context_view_on_map:
                intent = new Intent(getActivity(), AllEventsMapActivity.class);
                intent.putExtra("event", event);
                startActivity(intent);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void initialize() {
        list = (ListView) getActivity().findViewById(R.id.event_list);
        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                Event event = (Event) list.getItemAtPosition(position);

                Intent intent = new Intent(ListEventsFragment.this.getActivity(), EventDetailsActivity.class);
                intent.putExtra("event", event);
                startActivity(intent);
            }
        });

        list.setEmptyView(getActivity().findViewById(R.id.event_list_empty));
        registerForContextMenu(list);
    }

    /**
     * Load events from the server.
     * 
     * @author Emil Stjerneman
     * 
     */
    private class GetEventsTask extends RESTRequestTask<Void, Void, Event[]> {

        public GetEventsTask (Context context, String path) {
            super(context, path);
        }

        @Override
        protected Event[] doInBackground(Void... params) {
            super.doInBackground(params);
            Event[] events = null;

            try {
                events = restTemplate.getForObject(url, Event[].class);
            }
            catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }

            return events;
        }

        @Override
        protected void onPostExecute(Event[] events) {
            super.onPostExecute(events);

            if (events != null) {
                adapter = new EventListAdapter(context);
                list.setAdapter(adapter);

                for (Event e : events) {
                    adapter.add(e);
                }

            }
        }
    }
}
