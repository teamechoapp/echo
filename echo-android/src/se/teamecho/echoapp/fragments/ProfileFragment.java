package se.teamecho.echoapp.fragments;

import se.teamecho.echoapp.R;
import se.teamecho.echoapp.activity.ListEventsActivity;
import se.teamecho.echoapp.activity.LoginAcitivy;
import se.teamecho.echoapp.activity.ProfileActivity;
import se.teamecho.echoapp.domain.Profile;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;

public class ProfileFragment extends Fragment implements ConnectionCallbacks, OnConnectionFailedListener {


    private static final int RC_SIGN_IN = 0;
    private final static String TAG = "ProfileFragment";

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    /**
     * A flag indicating that a PendingIntent is in progress and prevents us
     * from starting further intents.
     */
    private boolean mIntentInProgress;

    private boolean mSignInClicked;

    private ConnectionResult mConnectionResult;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        View rootView = inflater.inflate(R.layout.profile, container, false);
        Button btn_profile_sign_out = (Button) rootView.findViewById(R.id.btn_profile_sign_out);
        Button btn_profile_revoke_access = (Button) rootView.findViewById(R.id.btn_profile_revoke_access);
        ImageView profile_image = (ImageView) rootView.findViewById(R.id.profile_image);
        TextView profile_name = (TextView) rootView.findViewById(R.id.profile_name);
        TextView profile_email = (TextView) rootView.findViewById(R.id.profile_email);

        Profile profile = Profile.getInstance();
        profile_name.setText(profile.getName());
        profile_email.setText(profile.getEmail());

        profile_image.setImageBitmap(profile.getImage());

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this).addApi(Plus.API, null)
        .addScope(Plus.SCOPE_PLUS_LOGIN).build();

        btn_profile_sign_out.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                signOutFromGplus();
            }
        });

        btn_profile_revoke_access.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                revokeGplusAccess();
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Try connecting with the previous credentials, if any.
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Sign-out from google
     */
    private void signOutFromGplus() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            startActivity(new Intent(getActivity(), LoginAcitivy.class));
        }
    }

    /**
     * Revoking access from google
     */
    private void revokeGplusAccess() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {

                @Override
                public void onResult(Status arg0) {
                    startActivity(new Intent(getActivity(), LoginAcitivy.class));
                }

            });
        }
    }

    /**
     * Method to resolve any sign in errors
     */
    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(getActivity(), RC_SIGN_IN);
            }
            catch (SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), getActivity(), 0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        mSignInClicked = false;
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

}
