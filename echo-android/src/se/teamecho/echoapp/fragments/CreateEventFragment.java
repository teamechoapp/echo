package se.teamecho.echoapp.fragments;

import org.joda.time.MutableDateTime;

import se.teamecho.echoapp.R;
import se.teamecho.echoapp.activity.CreateEventMapActivity;
import se.teamecho.echoapp.adapters.CategoryListAdapter;
import se.teamecho.echoapp.domain.Category;
import se.teamecho.echoapp.domain.Event;
import se.teamecho.echoapp.rest.GetCategoriesTask;
import se.teamecho.echoapp.rest.PostEventsTask;
import se.teamecho.echoapp.rest.PutEventsTask;
import se.teamecho.echoapp.utils.DateAndTime;
import se.teamecho.echoapp.utils.EventValidation;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.google.android.gms.maps.model.LatLng;

public class CreateEventFragment extends Fragment {

    private final static String TAG = "CreateEventFragment";

    public final static int REQUEST_CODE_MAP = 1;

    private Event original_event;

    private EditText event_name;
    private EditText event_description;

    private Spinner category_spinner;
    private CategoryListAdapter category_adapter;

    private EditText event_address;

    private LatLng event_location;

    private MutableDateTime event_start_date;
    private MutableDateTime event_end_date;

    private Button start_date_button;
    private Button end_date_button;
    private Button start_time_button;
    private Button end_time_button;

    private ImageButton locationButton;

    private Button saveButton;

    private boolean isEditMode = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.create_event, container, false);

        event_name = (EditText) rootView.findViewById(R.id.create_event_name);
        event_description = (EditText) rootView.findViewById(R.id.create_event_description);

        category_spinner = (Spinner) rootView.findViewById(R.id.create_event_category);

        event_address = (EditText) rootView.findViewById(R.id.create_event_address);

        start_date_button = (Button) rootView.findViewById(R.id.btn_create_event_start_date);
        end_date_button = (Button) rootView.findViewById(R.id.btn_create_event_end_date);

        start_time_button = (Button) rootView.findViewById(R.id.btn_create_event_start_time);
        end_time_button = (Button) rootView.findViewById(R.id.btn_create_event_end_time);

        locationButton = (ImageButton) rootView.findViewById(R.id.btn_create_event_location);

        saveButton = (Button) rootView.findViewById(R.id.btn_create_event_save);

        event_start_date = new MutableDateTime();
        event_end_date = new MutableDateTime();
        event_end_date.addHours(1);

        if (getActivity().getIntent().hasExtra("event") && !isEditMode) {
            isEditMode = true;
            populateEditData();
        }

        new GetCategoriesForSpinner().execute();

        initialize();

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_MAP:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle extras = data.getExtras();

                    if (data.hasExtra("location")) {
                        LatLng location = extras.getParcelable("location");
                        event_location = location;
                    }

                    if (data.hasExtra("address")) {
                        String address = extras.getString("address");
                        event_address.setText(address);
                    }
                }
                break;
        }
    }

    private void initialize() {

        start_date_button.setText(DateAndTime.getDateNiceName(event_start_date));
        end_date_button.setText(DateAndTime.getDateNiceName(event_end_date));

        start_time_button.setText(DateAndTime.getTimeNiceName(event_start_date));
        end_time_button.setText(DateAndTime.getTimeNiceName(event_end_date));

        start_date_button.setOnClickListener(new DateOnClickListener(event_start_date, start_date_button));
        end_date_button.setOnClickListener(new DateOnClickListener(event_end_date, end_date_button));

        start_time_button.setOnClickListener(new TimeOnClickListener(event_start_date, start_time_button));
        end_time_button.setOnClickListener(new TimeOnClickListener(event_end_date, end_time_button));

        locationButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreateEventMapActivity.class);
                if (event_location != null) {
                    intent.putExtra("location", event_location);

                    int index = category_spinner.getSelectedItemPosition();

                    if (index >= 0) {
                        Log.e("index", String.valueOf(index));
                        intent.putExtra("category", category_adapter.getItem(index));
                    }
                }
                startActivityForResult(intent, REQUEST_CODE_MAP);
            }
        });

        saveButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Event event = buildEvent();

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Someting is wrong");

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

                if (!EventValidation.eventIsValidated(event, isEditMode, builder)) {
                    return;
                }

                executeSaveAction(event);
            }
        });
    }

    /**
     * Grab data from the UI and build an event with it.
     * 
     * @return an event object.
     */
    private Event buildEvent() {
        // Get data
        String event_name_text = event_name.getText().toString();
        String event_description_text = event_description.getText().toString();
        String event_address_text = event_address.getText().toString();

        Category select_category = (Category) category_spinner.getSelectedItem();

        // Populate the new event object.
        Event event = new Event();
        event.setName(event_name_text);
        event.setDescription(event_description_text);
        event.setCategory(select_category);
        event.setStartDate(event_start_date.toString());
        event.setEndDate(event_end_date.toString());
        event.setAddress(event_address_text);
        if (original_event != null && original_event.getAttendees() != null) {
            event.setAttendees(original_event.getAttendees());
        }

        if (event_location != null) {
            event.setLongitude(event_location.longitude);
            event.setLatitude(event_location.latitude);
        }

        return event;
    }

    private void populateEditData() {
        getActivity().setTitle("Edit event");

        saveButton.setText("Update");

        Bundle extras = getActivity().getIntent().getExtras();
        original_event = (Event) extras.get("event");

        event_name.setText(original_event.getName());
        event_description.setText(original_event.getDescription());

        event_start_date = original_event.getStartDateDateTime().toMutableDateTime();
        event_end_date = original_event.getEndDateDateTime().toMutableDateTime();

        event_address.setText(original_event.getAddress());
        event_location = new LatLng(original_event.getLatitude(), original_event.getLongitude());
    }

    /**
     * Executes the save action. If in editMode, it will perform an Update,
     * otherwise a save.
     */
    private void executeSaveAction(Event event) {
        if (isEditMode) {
            new PutEventsTask(getActivity(), "events/" + original_event.getId()).execute(event);
        }
        else {
            new PostEventsTask(getActivity()).execute(event);
        }
    }

    /**
     * Handle the onclicks for date fields.
     * 
     * @author Emil Stjerneman
     * 
     */
    private class DateOnClickListener implements OnClickListener, DatePickerDialog.OnDateSetListener {

        private MutableDateTime source;
        private Button button;

        public DateOnClickListener (MutableDateTime source, Button button) {
            this.source = source;
            this.button = button;
        }

        @Override
        public void onClick(View v) {
            new DatePickerDialog(getActivity(), this, source.getYear(), source.getMonthOfYear() - 1, source.getDayOfMonth()).show();
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            source.setYear(year);
            source.setMonthOfYear(monthOfYear + 1);
            source.setDayOfMonth(dayOfMonth);

            button.setText(DateAndTime.getDateNiceName(source));
        }
    }

    /**
     * Handle the onclicks for time fields.
     * 
     * @author Emil Stjerneman
     * 
     */
    private class TimeOnClickListener implements OnClickListener, TimePickerDialog.OnTimeSetListener {

        private MutableDateTime source;
        private Button button;

        public TimeOnClickListener (MutableDateTime source, Button button) {
            this.source = source;
            this.button = button;
        }

        @Override
        public void onClick(View v) {
            new TimePickerDialog(getActivity(), this, source.getHourOfDay(), source.getMinuteOfHour(), true).show();
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            source.setHourOfDay(hourOfDay);
            source.setMinuteOfHour(minute);

            button.setText(DateAndTime.getTimeNiceName(source));
        }
    }

    /**
     * Loads categories from the server and populates the spinner with data.
     * 
     * @author Emil Stjerneman
     * 
     */
    private class GetCategoriesForSpinner extends GetCategoriesTask {

        public GetCategoriesForSpinner () {
            super(getActivity());
        }

        @Override
        protected void onPostExecute(Category[] categories) {
            super.onPostExecute(categories);

            if (categories != null) {
                category_adapter = new CategoryListAdapter(getActivity(), R.layout.event_category_item, R.id.category_spinner_item_name);
                category_spinner.setAdapter(category_adapter);

                for (Category category : categories) {
                    category_adapter.add(category);
                    if (isEditMode && category.getId().equals(original_event.getCategory().getId())) {
                        category_spinner.setSelection(category_adapter.getCount() - 1);
                    }
                }
            }
        }
    }
}
